<?php

namespace Wikisuite\ILPSPSP;

class ILPSPSPClient
{
    use InvoiceTrait;

    private static ILPSPSPClient $instance;

    private InvoiceServerConfig $config;

    /**
     * ILPSPSPClient constructor.
     * @param InvoiceServerConfig $config
     */
    private function __construct(InvoiceServerConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @param InvoiceServerConfig $config
     */
    public static function create(InvoiceServerConfig  $config){
        self::$instance = new ILPSPSPClient($config);
    }

    /**
     * @return InvoiceServerConfig
     */
    public function getConfig(): InvoiceServerConfig
    {
        return $this->config;
    }

    /**
     * @return ILPSPSPClient
     */
    public static function getInstance()
    {
        return self::$instance;
    }

}