<?php

namespace Wikisuite\ILPSPSP;

class InvoiceServerConfig
{
    private $baseURL;

    private $token;
    private bool $ssl;

    /**
     * InvoiceServerConfig constructor.
     * @param $baseURL
     * @param $token
     * @param bool $ssl
     */
    public function __construct($baseURL, $token, $ssl = true)
    {
        $this->baseURL = $baseURL;
        $this->token = $token;
        $this->ssl = $ssl;
    }

    /**
     * @return mixed
     */
    public function getBaseURL()
    {
        return $this->baseURL;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return bool
     */
    public function isSsl(): bool
    {
        return $this->ssl;
    }

}