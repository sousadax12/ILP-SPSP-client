<?php

namespace Wikisuite\ILPSPSP;

trait PointerTrait
{
    public function resolvePointer($pointer, $ssl = true)
    {
        return substr($pointer, 0, 1) == '$' ? ($ssl ? 'https' : 'http') . '://' . substr($pointer, 1) : $pointer;
    }
}