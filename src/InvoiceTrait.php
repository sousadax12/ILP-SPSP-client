<?php

namespace Wikisuite\ILPSPSP;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\PropertyAccess\PropertyAccess;

trait InvoiceTrait
{
    use PointerTrait;

    /**
     * @param $amount
     * @param $assetCode
     * @param $assetScale
     * @param string $webhook
     * @param array $extraFields
     * @return string Pointer to invoice
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function createInvoice(
        $amount,
        $assetCode,
        $assetScale,
        $webhook = "",
        $extraFields = []
    ): string
    {
        $client = ILPSPSPClient::getInstance();
        $config = $client->getConfig();

        $params = array_merge($extraFields, [
            'amount' => $amount,
            'assetCode' => $assetCode,
            'assetScale' => $assetScale,
            'webhook' => $webhook,
        ]);


        $client = HttpClient::create();
        $response = $client->request(
            'POST',
            trim($config->getBaseURL(), '/'),
            [
                'auth_bearer' => $config->getToken(),
                'body' => $params
            ]
        );
        $data = $response->toArray(true);
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        return $this->resolvePointer($propertyAccessor->getValue($data, "[invoice]"), $config->isSsl());
    }


    public function getInvoice($pointer)
    {
        $client = ILPSPSPClient::getInstance();
        $config = $client->getConfig();
        $url = $this->resolvePointer($pointer, $config->isSsl());

        $client = HttpClient::create();
        $response = $client->request(
            'GET',
            $url,
            [
                'auth_bearer' => $config->getToken(),
                'headers' => [
                    'Accept' => 'application/spsp4+json, application/spsp+json'
                ],
            ]
        );
        return $this->jsonToInvoice($response->toArray(true));

    }


    private function jsonToInvoice($data)
    {
        $invoice = new Invoice();
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $invoice->setDestinationAccount($propertyAccessor->getValue($data, '[destination_account]'));
        $invoice->setSharedSecret($propertyAccessor->getValue($data, '[shared_secret]'));
        $invoice->setBalance($propertyAccessor->getValue($data, '[push][balance]'));
        $invoice->setAmount($propertyAccessor->getValue($data, '[push][invoice][amount]'));
        $invoice->setAssetCode($propertyAccessor->getValue($data, '[push][invoice][asset][code]'));
        $invoice->setCodeScale($propertyAccessor->getValue($data, '[push][invoice][asset][scale]'));
        $invoice->setAdditionalFields($propertyAccessor->getValue($data, '[push][invoice][additional_fields]'));
        return $invoice;
    }

}