<?php

use PHPUnit\Framework\TestCase;
use Symfony\Component\Process\Process;
use Wikisuite\ILPSPSP\ILPSPSPClient;
use Wikisuite\ILPSPSP\Invoice;
use Wikisuite\ILPSPSP\InvoiceServerConfig;
use Wikisuite\ILPSPSP\PointerTrait;

class CreateInvoiceTest extends TestCase
{
    use PointerTrait;

    public function testCreateInvoice()
    {
        ILPSPSPClient::create(new InvoiceServerConfig('http://localhost:6000', 'test', false));
        $client = ILPSPSPClient::getInstance();
        $amount = "900";
        $assetCode = "XRP";
        $assetScale = "9";
        $response = $client->createInvoice($amount, $assetCode, $assetScale, "", ["reason" => "more stuff"]);
        $this->assertIsString($response);

        $invoice = $client->getInvoice($response);
        $this->assertInstanceOf(Invoice::class, $invoice);

        $this->assertEquals($invoice->getAmount(), $amount);
        $this->assertEquals($invoice->getAssetCode(), $assetCode);
        $this->assertEquals($invoice->getCodeScale(), $assetScale);
        $this->assertIsArray($invoice->getAdditionalFields());
        $this->assertNotEmpty($invoice->getAdditionalFields());
        $this->assertIsString($invoice->getDestinationAccount());
        $this->assertIsNumeric($invoice->getBalance());
        $this->assertIsString($invoice->getSharedSecret());

        $this->assertFalse($invoice->isPayed());
        $url = $this->resolvePointer($response, false);
        $process = new Process(["ilp-spsp", "send", "-a", $invoice->getAmount(), "-p", $url]);
        $process->run();
        $invoice = $client->getInvoice($response);
        $this->assertTrue($invoice->isPayed());

    }


}
